import json
import os
with open('solution.json' ,"r") as file:
  data=json.load(file)

member_ids=[1234,
            3456,
            4567,
            2341,
            3345,]

qry_prm="?sln_id=1234&offset=2&limit=3"



def solutions(qry_prm,member_ids, *solutions):
    d=dict()
    qry_prm=qry_prm[1:]
    qry_prm=qry_prm.split("&")
    for i in qry_prm:
        k,v=(i.split("="))
        d[k]=int(v)


    offset=0
    limit=0
    total_count=0
    solutionids=[]
    installed_solutions=0
     
    if 'offset' in d:
        offset=int(d['offset'])

    if 'limit' in d:
         limit=int(d['limit'])
    
    solutionids=member_ids[offset:offset+limit]

    total_count=len(member_ids)
    
    for solution in solutions:
        solutionid=solution["solutionId"]
        if "solutionId" in solution:
            installed_solutions+=1
            
    
    expected_response={
        "solutionId":solutionids,
        "installed_solutions":installed_solutions,
        "offset":offset,
        "total_count":total_count
    }
    print(expected_response)
solutions(qry_prm,member_ids,data)